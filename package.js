Package.describe({
  name:          '37degrees:local-auth',
  version:       '0.0.1',
  // Brief, one-line summary of the package.
  summary:       '',
  // URL to the Git repository containing the source code for this package.
  git:           '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'meteor-base',
    'check',
    'accounts-base',
    'accounts-password',
    '37degrees:mobile-utils',
    'alanning:roles@1.2.14',
    'mizzao:accounts-testing@0.1.0'
  ]);

  api.imply([
    'check',
    'accounts-base',
    'accounts-password',
    'alanning:roles'
  ]);

  api.addFiles('lib/init/localAuth.js', ['client', 'server']);

  api.addFiles([
    'client/helpers/localLogin.js',
    'client/guest.js',
    'client/localUser.js'
  ], 'client');

  api.addFiles('server/localLogin.js', 'server');

  api.export('d37localAuth', ['server', 'client']);
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('local-auth');
  api.addFiles('localAuthTests.js');
});
