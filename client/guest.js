d37localAuth.loginAsGuest = function (callback) {
  var pfx  = "d37localAuth.loginAsGuest",
      user = Meteor.user(),
      userId;

  if (!user) {
    d37localAuth.createLocalUser(function (err) {
      if (err) {
        console.log(pfx + "createLocalUser error", { error: err });
        callback && callback();
        return;
      }

      userId = Meteor.userId();
      if (userId && !Meteor.loggingIn()) {
        console.log(pfx + "createLocalUser callback", { userId: userId });
        callback && callback(userId);
      }
    });
  } else {
    console.log(pfx + "Already have user");
    callback && callback(Meteor.userId());
  }
};
