import { MobileUtils } from 'meteor/37degrees:mobile-utils';

d37localAuth.createLocalUser = function (callback) {
  var pfx       = "[d37localAuth.createLocalUser] ",
      location  = Location && Location.getLastPosition ? Location.getLastPosition() : null,
      d37mobile = new MobileUtils(),
      profile,
      deviceId,
      userId;

  // First clear all data for current session
  d37localAuth.clearSession();

  profile = {
    'device':   d37mobile.device,
    'name':     'Guest',
    'timezone': TimezonePicker.detectedZone()
  };

  deviceId = d37mobile.device.uuid;

  console.log(pfx + "Meteor.localLogin", { deviceId: deviceId, profile: profile });

  Meteor.localLogin(deviceId, profile, function (err) {
    if (err) {
      console.log(pfx + "Meteor.localLogin Error",
        { error: err, deviceId: deviceId, profile: profile, location: location });
      callback && callback(err);
      return;
    }

    // Ensure we have a Meteor user
    userId = Meteor.userId();
    if (!userId) {
      console.log(pfx + 'Meteor.localLogin: No user ID returned', { profile: profile, location: location });
      callback && callback(err);
      return;
    }

    Meteor.call("addRolesToUser", ['guest']);

    console.log(pfx + "Meteor.localLogin called", { deviceId: deviceId, profile: profile });

    callback && callback();
  });
};
