// NOTE: this requires the following package: mizzao:accounts-testing
// It calls the server method "Accounts.registerLoginHandler"
Meteor.localLogin = function (username, profile, callback) {
  Accounts.callLoginMethod({
    methodArguments: [{ local: true, username: username, profile: profile }],
    userCallback:    callback
  });
};
